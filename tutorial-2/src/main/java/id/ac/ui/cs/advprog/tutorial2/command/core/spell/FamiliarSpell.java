package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.FamiliarState;

import java.util.List;

public abstract class FamiliarSpell implements Spell {
    protected Familiar familiar;
    // TODO: Complete Me
    public FamiliarSpell(Familiar familiar){
        this.familiar = familiar;
    }

    @Override
    public void undo() {
        if (familiar.getPrevState() == FamiliarState.ACTIVE) familiar.summon();
        // TODO: Complete Me
        else if (familiar.getPrevState() == FamiliarState.SEALED) familiar.seal();

        List<String> lifeArchive = familiar.getLifeArchive();
        try{
            lifeArchive.remove(lifeArchive.size() - 1);
            lifeArchive.remove(lifeArchive.size() - 1);
        }
        catch(IndexOutOfBoundsException ex){
            System.out.println("No spells have been casted on this spirit");
        }
    }
}

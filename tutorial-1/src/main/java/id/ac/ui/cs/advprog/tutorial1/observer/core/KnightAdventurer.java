package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {

    public KnightAdventurer(Guild guild) {
        super();
        this.name = "Knight Adventurer";
        this.guild = guild;
    }

    @Override
    public void update(){
        String questType = guild.getQuestType();
        if(questType == "D" || questType == "E" || questType == "R"){
            getQuests().add(guild.getQuest());
        }
    }
}

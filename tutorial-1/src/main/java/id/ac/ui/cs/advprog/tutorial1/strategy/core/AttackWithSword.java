package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    @Override
    public String getType(){
        return "S";
    }

    @Override
    public String attack(){
        return "Attack with Sword!";
    }
}

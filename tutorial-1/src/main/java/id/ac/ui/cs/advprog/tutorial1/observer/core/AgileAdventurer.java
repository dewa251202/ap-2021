package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

    public AgileAdventurer(Guild guild) {
        super();
        this.name = "Agile Adventurer";
        this.guild = guild;
    }

    @Override
    public void update(){
        String questType = guild.getQuestType();
        if(questType == "D" || questType == "R"){
            getQuests().add(guild.getQuest());
        }
    }
}

package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

    public MysticAdventurer(Guild guild) {
        super();
        this.name = "Mystic Adventurer";
        this.guild = guild;
    }

    @Override
    public void update(){
        String questType = guild.getQuestType();
        if(questType == "D" || questType == "E"){
            getQuests().add(guild.getQuest());
        }
    }
}
